# Pismeni ispiti i koloviji

ovdje se nalaze svi pismeni ispiti. Dodan je i primjeri.md file koji pokazuje predložena rješenja. 

Predložena rješenja su apsolutni maksimum koji se od vas očekuje na ispitu, ne morate ići dublje u detalje implementacije, bitno je pokazati da znate razraditi high-level funkcionalnosti i kako ćete iz pojedinog dijela pristupiti podacima u drugom dijelu.

