# Primjer pismenog ispita za 2425

## Primjer 1: Teži i više se integrira u postojeći projekt

**Requirements**: Omogućiti sustav za Referrals. 

- Moguće je generirati svoj Referral link i poslati ga korisnicima. Link vodi na registraciju.
- Link se generira klikom na tipku u User Dashboardu
- Korisnik u Dashboardu vidi svoje Referrale
- Novi korisnik (REFERRED) kada se registrira preko Refferal linka dobije probni iznos 20 tokena na račun. 
- Korisnik koji ga je pozvao (REFERER) dobije nagradu 50 tokena nakon što REFERRED potroši 100 tokena.

## Predloženo rješenje


#### Modeliranje podataka

`Account` model: 

- Dodatni podaci za korisnike prate se u `Account` modelu. 
    - Dodati field `referral_code` (null dozvoljen) u model `Account`.  
    - Dodati field `referral` FK na `Referral` (one-to-one field, null dozvoljen)
- Dodati metodu `generate_referral_code` u model koja generira i sprema kod
- Dodati metodu `get_referral_link` u `Account` model koja će vratiti link oblika `localhost:8000/accounts/registration/?ref={referral_code}`.

Novi `Referral` model:

```python
referred_by = FK na Account #Korisnik koji je pozvao i preko čijeg linka se registrira
code = referral_code (string 8 znakova) #Iskorišteni kod. Referral kod ćemo spremati kao 8 random alfanumeričkih znakova.
referred_tokens_awarded = decimal, null dozvoljen #postavlja se kada se referred registira
referer_tokens_awarded = decimal, null dovoljen #postavlja se kada se dodijele tokeni refereru
```

- pamtimo iskorišteni kod jer REFERER može promijeniti svoj kod u nekom trenutku, pa nam je bitno pamtiti koji je kod korišten za koji `Referral`

#### Views i templates

Novi view: 

```python
# url: /accounts/generate_referral_code/ (POST) name=generate_referral_code
# views: accounts/views.py
def generate_referral_code(request): #Prima samo request
    pass
    # Identificira korisnika preko request.user
    # zove user.account.generate_referral_code
    # redirecta nazad na User dashboard
```

Update dashboard templatea: template prikazuje link za generiranje koda ako ne postoji, ili referral link ako kod već postoji

```html
    <!-- ubaciti negdje u dashboard -->
    IF not user.account.referral_code
        <p><a href="URL za generate_referral_code">Generate Referral Code</a></p>
    ELSE
        <p>
            <a href="{{ user.account.get_referral_link }}">
                Ovo je vaš pozivni link, podijelite ga
            </a>
        </p>
    END

    <!-- ubaciti negdje u dashboard objašnjenje što referrali znače i kako se koriste -->

    <!-- ubaciti negdje u dashboard --> 
    Invited users:
        TABLICA svih usera registriranih preko referral linka korisnika

    Completed referrals:
        TABLICA svih završenih referrala (REFERRER dobio svojih 50 tokena)
```

**Update `accounts/views.py -> registration` viewa**:

- u formu dodati novo polje za Account: referral_code
- view na GET čita iz GET/QUERY parametara kod i ubacuje ga u template u taj input
- na templateu u formi se renderira novo polje `Referral Code` koje se automatski popuni ako se na registraciju dođe preko linka, ali dozvoljava i korisniku da sam unese kod.
- Ako je kod neispravan ili nepostojeći, rendera ponovno formu s porukom da je kod neispravan.
- Ako je user uspješno registriran, view prije redirecta kao zadnji korak treba:
    - Kreirati novi `referral = Referral()` zapis i spremiti u bazu:
        - objekt prima `referer` koji pronađemo tako da filtriramo accounte koji imaju taj `referral_code`
        - objekt prima `referred` koji je user objekt (user koji se upravo registrirao)
        - objekt prima `code` koji je kod izvučen iz parametara forme   
    - Updatea `Account` objekt novokreiranog usera i povezuje upravo kreirani `referral` objekt
    - pozvati `referral.award_tokens_for_registration` koji će dodijeliti tokene REFERRED useru i updateati `referred_tokens_awarded`
        - implementirati tu metodu na modelu


**Update `app/views.py -> job_completed` viewa**:

- Kada korisnik potvrdi da je JOB izvršen, u trenutku kada se prebacuju tokeni:
    - poziva se se `user.account.check_referral_award`. Metoda se dodaje u Account model.
    - ova metoda treba provjeriti: 
        - Je li korisnik došao preko referrala
        - Je li referral već završen (dodijeljeni tokeni oba korisnika).
        - Je li završavanjem ovog posla korisnik potrošio >= 100 tokena
        - Ako uvjeti valjaju: dodijeljuju se nagradni tokeni useru koji ga je pozvao
        - updatea se polje u `Referral` o dodijeljenoj nagradi



# Primjer 2: Lakši i neovisniji o postojećim funkcionalnostima

**Requirements**: Omogućiti ankete

- registriranim korisnicima se na homepagu, nakon potrošenih >=50 tokena prikaže link za ispunjavanje ankete
- sadrži:
    - Pitanja o zadovoljstvu
    - Vaš komentar
- Pitanja: 
    - Kako ste zadovoljni s korištenjem portala (1-5)
    - Biste li ga preporučili drugim korisnicima (1-5)
    - napomena: ocjena pitanja je uvijek 1-5, ali broj pitanja se može mijenjati. Predvidjeti tu opciju, gdje će se dodavati nova pitanja.
- Anketa se sprema nakon ispunjavanja
- Korisnik koji je ispunio anketu ne vidi više link na nju
- Admini imaju dodatno sučelje za pregled ispunjenih anketa


## Predloženo rješenje

### Modeliranje podataka

Dodati modele

```python
class Poll
    account = FK na Account
    comment = text


class Question
    question = text

class Response
    poll = FK na Poll
    question = FK na Question
    grade = integer
```

Dodati u model `Account`:

```python
def poll_available(self):
    pass
    # Treba provjeriti je li korisnik već ispunio anketu
    # Treba zbrojiti korisnikove prihvaćene bidove
    # ako je zbroj >= 50 vraća True, inače vraća False
```

##### REST

```http
/app/polls/new -> views.polls_new (GET render, POST sprema)
/app/polls/<int:poll_id> -> views.polls_detail (GET render)
/app/polls/dashboard -> views.polls_dashboard (GET render)
```

##### Views and templates

**Dodati u `home.html`:**

```html
<!-- negdje na homepage -->
IF user.request.account.poll_available
    LINK na formu za ispunjavanje ankete
```

**Dodati u `app/views.py`:**

```python
def polls_new(request):
    pass
    # Nema dodatnih parametara na GET, identificira usera po request.user
    # Provjerava `poll_available` za svaki slučaj, ako korisnik dođe direkt preko linka
    # Ako je `poll_available` na GET rendera formu
    # Na POST forme kreira Poll objekt i dodaje komentar
    # - Za svaku ocjenu u formi kreira Response povezan na kreirani poll i ubacuje ocjenu
```

**Template za `templates/new_poll.html`:**

```html
<!-- novi file, standardni extends i ostalo -->
Forma: 
    - for question in questions (proslijeđeno iz viewa sa Question.objects.all)
        - tekst pitanja
        - input id="question_{question.id}"
    
    - comment input
    - submit
```

**Dodati u `app/views.py`:**

```python
def polls_detail(request, poll_id):
    pass
    # get_object_or_404(poll_id)
    # ubaci u kontext
    # Render templatea polls_detail.html
```


**Template za `templates/polls_detail.html`**:

```html
<!-- novi file, standardni extends i ostalo -->
Podaci o useri i anketi

for response in poll.response_set.all
    Pitanje: {{ response.question.question }}
    Ocjena: {{ response.grade}}

Komentar:
{{ poll.comment }}

```

**Dodati u `app/polls_dashboard.py`:**

```python
def polls_dashboard(request):
    pass
    # Provjeri je li request.user.is_superuser
    # povuče sve ankete sa Poll.objects.all()
    # ubaci u kontext
    # render polls_dashboard/html
```

**Template za `templates/polls_dashboard.html`:**

```html
<!-- novi file, standardni extends i ostalo -->
TABLICA:
    - podaci o useru
    - ocjene
    - link na polls_detail
```


