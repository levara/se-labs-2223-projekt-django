# Procedura predaje zadataka na pismenom ispitu

Nakon što su vam nastavnici pregledali zadatke, možete ih postaviti na GitLab.

1. Prijavite se na svoj korisnički račun na GitLabu.
2. U gornjem desnom uglu kliknite na `+` i odaberite `New project`.
3. Odaberite `Create blank project`.
4. Unesite ime projekta u formatu `ime.prezime.2324-N` i odaberite `Private` za vidljivost.
   - `N` u ovom slučaju predstavlja broj redovnog roka: `1` za prvi zimski, `2` za drugi zimski, `3` za prvi ljetni, itd...
5. Za opciju `Project deployment target` odaberite `No deployment planned`.
6. Isključite opciju `Initialize repository with a README` i kliknite na `Create project`.
7. Kada je projekt kreiran, pod opcijom `Project information` kliknite na `Members`.
8. U gornjem desnom uglu kliknite na `Invite members`.
9. Dodajte account `hleventic` kao `Owner`. Ponudit će vam  `hleventic` i `hleventic-ferit` accounte, dodajete `hleventic`.
10. U terminalu dodajte novi remote za svoj projekt:
   ```bash
   git remote add ispit URL_VAŠEG_NOVOG_REPOZITORIJA
   ```
11. Pushajte svoj kod na GitLab:
   ```bash
   git push ispit master
   ```
